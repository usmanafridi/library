import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import {createDrawerNavigator } from 'react-navigation-drawer'
import {RecoilRoot} from 'recoil';
import UserRegister from './appScreens/user/userRegister';
import Login from './appScreens/user/login';
import Index from './appScreens/dashboard';
import IndexDetail from './appScreens/dashboard/indexDetail';
import ReservedBook from './appScreens/dashboard/reservedBooks';
import ContactUs from './appScreens/sidedrawer/contactUs';
import IssudBooks from './appScreens/dashboard/issuedBooks';
import SideDrawer from './appScreens/sidedrawer/drawer';
import Splash from './appScreens/splashScreen';
import EditDetail from './appScreens/sidedrawer/editPersonalDtl';

AppNavigation = createStackNavigator({
  splash:{
    screen: Splash,
    navigationOptions: {
      headerShown: false
    }
  },
  login: {
    screen: Login,
    navigationOptions: {
      headerShown: false
    }
  },
  register: {
    screen: UserRegister,
    navigationOptions:{
      headerShown: false
    }
  },
  index:{
    screen: Index,
    navigationOptions: {
      headerShown: false
    }
  },

  reserved:{
    screen: ReservedBook,
    navigationOptions: {
      headerShown: false
    }
  },
  issued:{
    screen: IssudBooks,
    navigationOptions: {
      headerShown: false
    }
  },
  drawer: {
    screen: SideDrawer,
      navigationOptions: {
      headerShown: false
    }
  },
  indexDtl: {
    screen: IndexDetail, 
    navigationOptions: {
      headerShown: false
    }
  },
  contact: {
    screen: ContactUs,
    navigationOptions: {
      headerShown: false
    }
  },
  editDetail: {
    screen: EditDetail,
    navigationOptions: {
      headerShown: false
    }
  },
 

})

// const Appcontainer = createAppContainer(AppNavigation);
const Appcontainer = createAppContainer(
  createDrawerNavigator({
    navigation:{screen:AppNavigation}
  },{
    contentComponent: SideDrawer,
    drawerBackgroundColor:'#fff'
  })
)
 const App = () => {
    return (
      <RecoilRoot>
        <Appcontainer></Appcontainer>
      </RecoilRoot>
    );
  }
export default App;