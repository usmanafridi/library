import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, TouchableOpacity, Dimensions, StatusBar, Alert,ImageBackground, Image, AsyncStorage } from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { Dropdown } from 'react-native-material-dropdown-v2';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { userStyle } from '../assets/style/styles';
import CustomInput from '../component/textInput';
import { backGroundColor } from '../assets/colors/colors';
import {API_URL} from '../../constants';


const HEIGHT = Dimensions.get('window').height;

const UserRegister = ({ navigation }) => {

  const [loading, setLoading] = React.useState(false)
  const [name, setName] = useState(null)
  const [email, setEmail] = useState(null)
  const [phone, setPhone] = useState(null)
  const [organization, setOrganization] = useState(null)
  const [password, setPassword] = useState(null)
  const [lastId , setLastId] = useState(0)
  useEffect(()=>{
    getLastId()

    
},[lastId])

  const getLastId = () => {

    fetch(API_URL + 'values/SingleList', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })

        .then((response) => response.json())
        .then((responseJson) => {
            let alpha = responseJson.id.charAt(0)
            let digit = responseJson.id.slice(1)
            let sum = parseInt(digit) + parseInt(1)
            setLastId(alpha+sum)
            // alert(lastId)
                     
        })
        .catch((error) => {
           
        })
}


const saveUser = async (key, obj) =>{
  try {
    await AsyncStorage.setItem(key, obj)

  }catch (error) {

  }
}
 const validate = (text) => {
  console.log(text);
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(text) === false) {
    alert("Email is invalid");
    return false;
  }
  else {
    signUpHandle()
    return true;
  }
}

const signUpHandle = () => {

    if (name === null || password === null ||
         email === null || organization === null || phone === null ) {
       alert(lastId)
        Alert.alert(
            'Wrong Input!',
            'Fields cannot be empty.',
            [{ text: 'Okay' }],
        );
    }
    else {
      let formData = new FormData();
      formData.append('Name',name);
      formData.append('Password',password);
      formData.append('Organization',organization);
      formData.append('Mobile',phone);
      formData.append('Email',email);
      formData.append('MemberCode',lastId);


        setLoading(true)
        fetch('http://175.107.63.137/LibraryAPP/api/values/SignUp', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data'
          },
         body:formData
        }).then((response) => response.json())
        .then((responseData) => {

            // setLoading(false)  
            console.log(responseData.message)
            if(responseData.message === 'Data successfully inserted '){
              let obj = {
                auth: true,
                memberId: responseData.data.memberCode,
                name: responseData.data.name,
                
              }
            //  saveUser('storeUser', JSON.stringify(obj))
             setLoading(false)
             navigation.navigate('index')
            }else{ 
              alert('Something went wrong please try agian later') 
              setLoading(false)
            }
                      
          })
             .catch((error) => {
                setLoading(false)
                alert('Network request failed! Please check your internet connection')
          })
    }

}

  return (
    <ImageBackground
      style={{ height: '100%', width: '100%' }}
      source={require('../assets/images/background1.jpg')}
    >
      <ScrollView style={userStyle.container}>

        <StatusBar backgroundColor={'darkgray'} ></StatusBar>

        <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', height: HEIGHT / 2.9, }}>
          <Image
            style={{ width: 400, height: HEIGHT / 5, }}
            source={require('../assets/images/signup.png')} />
        </View>

        <View style={{ flex: 1 }}>
          <CustomInput
            icon='person'
            customStyle={{ }}
            hint='Name'
            keyboard=''
            multiline={false}
            pass={false}
            textAlign='center'
            onChange={(val) => setName(val)}
          >
          </CustomInput>

          <CustomInput
            icon='mail'
            customStyle={{ }}
            hint='Email'
            keyboard='email-address'
            multiline={false}
            pass={false}
            textAlign='center'
            onChange={(val)=>setEmail(val)}
          ></CustomInput>

          <CustomInput
            icon='call'
            customStyle={{ }}
            hint='Phone'
            keyboard='phone-pad'
            multiline={false}
            pass={false}
            textAlign='center'
            onChange={(val) => setPhone(val)}
          ></CustomInput>

          <CustomInput
            icon='home'
            customStyle={{}}
            hint='Organisation'
            keyboard=''
            multiline={false}
            pass={false}
            textAlign='center'
            onChange={(val) => setOrganization(val)}
          ></CustomInput>

          <CustomInput
            icon='lock-closed'
            customStyle={{}}
            hint='Password'
            keyboard=''
            multiline={false}
            pass={true}
            textAlign='center'
            onChange={(val) => setPassword(val)}
          ></CustomInput>
        </View>

        <View style={{ width: '100%', height: 80, justifyContent: 'center', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => validate(email)}
          style={{ width: 120, height: 50, justifyContent: 'center', alignItems: 'center', backgroundColor: 'orange', borderRadius: 10 }}>
            {loading === true ? null :
            <Text style={{ color: '#fff' }}>SIGN UP</Text>}
          </TouchableOpacity>
        </View>

        <Text style={{ color: '#fff', alignSelf: 'center' }}>Term of use-privacy policy</Text>
        <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: '#fff', alignSelf: 'center' }}>Already account! </Text>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
          >
            <Text style={{ color: '#fff', alignSelf: 'center' }}>LOGIN </Text>
          </TouchableOpacity>
        </View>

      </ScrollView>
    </ImageBackground>
  );
};

export default UserRegister;
