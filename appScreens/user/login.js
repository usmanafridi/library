import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, TouchableOpacity, Dimensions, StatusBar, ImageBackground, Image, Alert, AsyncStorage } from 'react-native';
import { userStyle } from '../assets/style/styles';
import CustomInput from '../component/textInput';
import { backGroundColor } from '../assets/colors/colors';
import {API_URL} from '../../constants';
import {StackActions, NavigationActions} from 'react-navigation';


const HEIGHT = Dimensions.get('window').height;

const Login = ({ navigation }) => {

    const [loading, setLoginLoading] = React.useState(false)
    const [phone, setPhone] = useState('')
    const [password, setPassword] = useState('')

    useEffect(()=>{
    
    }, [loading])

    const saveUser = async (key, obj) =>{
        try {
          await AsyncStorage.setItem(key, obj)
      
        }catch (error) {
      
        }
      }
      const _getStoregetData = async () => {
        try {
          const value = await AsyncStorage.getItem('storeUser');
          var res = JSON.parse(value);
        
          if (value !== null) {
            
            if (res.auth === true) {
    
              setUserId(res.memberCode);
              setUserName(res.name);
              const resetAction = StackActions.reset({
                index: 0,
                key: null,
                actions: [NavigationActions.navigate({ routeName: 'index' })],
              });
              navigation.dispatch(resetAction);
            //   navigation.navigate('index')
    
            }
    
          } else {
            
            const  resetAction = await StackActions.reset({
              index: 0,
              key: null,
              actions: [NavigationActions.navigate({ routeName: 'index' })],
            });
            navigation.dispatch(resetAction);
            
          }
        }
        catch (error) {
          console.log("Exception =" + error);
        }
      }

    const loginHandle = () => {
        if (phone === '' || password === '') {
            Alert.alert(
                'Wrong Input!',
                'Phone or password field cannot be empty.',
                [{ text: 'Okay' }],
            );
        }else {
            let formData = new FormData();
            formData.append('Mobile',phone);
            formData.append('Password',password);

            setLoginLoading(true)
            fetch(API_URL+'values/Login', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
              },
              body:formData
            })
      
              .then((response) => response.json())
              .then((responseData) => {
                if (responseData === null) {
                    alert('Invalid phone or password')
                    setLoginLoading(false)
                }else {
                    let obj = {
                        auth: true,
                        memberCode: responseData.id,
                        name: responseData.firstName,
                        
                      }
                     saveUser('storeUser', JSON.stringify(obj))
                     setLoginLoading(false)
                      _getStoregetData()
                     const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'splash' })],
                    });
                    navigation.dispatch(resetAction);
                }
                          
              })
              .catch((error) => {
                  alert(error)
                setLoginLoading(false)
              })
        }

    }




    return (
        <ImageBackground
            style={{ height: '100%', width: '100%' }}
            source={require('../assets/images/background1.jpg')}
        >
            <ScrollView>

                <StatusBar backgroundColor = {'darkgray'} ></StatusBar>

                <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', height: HEIGHT / 2.9, }}>
                    <Image 
                        style = {{width: 400, height: HEIGHT/5,}}
                        source={require('../assets/images/signin.png')} />
                </View>

                <View style={{ flex: 1, }}>
                    <CustomInput
                        hint='Phone'
                        keyboard='phone-pad'
                        icon = 'call'
                        onChange={(val) => setPhone(val)}
                    ></CustomInput>

                    <CustomInput
                        hint='Password'
                        pass={true}
                        icon = 'lock-closed'
                        onChange={(val) => setPassword(val)}
                    ></CustomInput>
                </View>

                <View style={{ width: '100%', height: 80, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity 
                    onPress={() => {
                        loginHandle();}}
                    style={{ width: 120, height: 50, justifyContent: 'center', alignItems: 'center', backgroundColor: 'orange', borderRadius: 10 }}>
                        <Text style = {{color: '#fff'}}>LOGIN</Text>
                    </TouchableOpacity>
                </View>

                <View style={{flexDirection: 'row', width: '100%', justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{ color: '#fff', alignSelf: 'center' }}>Don't have an account? </Text>
                    <TouchableOpacity 
                        onPress={()=>navigation.navigate('register')}
                    >
                        <Text style={{color: '#fff', alignSelf: 'center'}}>SIGN UP </Text>
                    </TouchableOpacity>
                </View>
                <Text style={{ color: '#fff', alignSelf: 'center' }}>Trouble Signing in? </Text>



            </ScrollView>
        </ImageBackground>
    );
};

export default Login;
