import {atom} from 'recoil';

export const userIdAtom = atom({
    key: 'USERID',
    default: '0'
})


export const NameAtom = atom({
    key: 'FULLNAME',
    default: '0'
});

export const BooksArrayAtom = atom({
    key: 'Books',
    default: []
});

