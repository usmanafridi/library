import React, { useEffect, useState} from 'react';
import {View, Text, AsyncStorage} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';
import {NameAtom, userIdAtom} from '../appScreens/recoil/atom';
import {useRecoilState} from 'recoil';

const Splash = ({navigation}) =>{
  
    const [userName, setUserName] = useRecoilState(NameAtom)
    const [userId, setUserId] = useRecoilState(userIdAtom)

   
    useEffect(() => {
      _getStoregetData()
    }, [userName, userId])


      const _getStoregetData = async () => {
        try {
          const value = await AsyncStorage.getItem('storeUser');
          var res = JSON.parse(value);
        
          if (value !== null) {
            
            if (res.auth === true) {
    
              setUserId(res.memberCode);
              setUserName(res.name);
              const resetAction = await StackActions.reset({
                index: 0,
                key: null,
                actions: [NavigationActions.navigate({ routeName: 'index' })],
              });
              navigation.dispatch(resetAction);
              // navigation.navigate('index')
    
            }
    
          } else {
            
            const  resetAction = await StackActions.reset({
              index: 0,
              key: null,
              actions: [NavigationActions.navigate({ routeName: 'login' })],
            });
            navigation.dispatch(resetAction);
            
          }
        }
        catch (error) {
          console.log("Exception =" + error);
        }
      }

    return(
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        
            <Text style={{fontSize: 20, fontWeight: 'bold', color: '#000'}}>Welcome to library</Text>
        </View>
    )
}

export default Splash;