import React, {  } from 'react';
import { View, Text, } from 'react-native';
import GeneralStatusBarColor from '../component/statusbar';
import { backGroundColor } from '../assets/colors/colors';
import BooksComponent from '../component/booksComponent';
import { NameAtom } from '../recoil/atom';
import {useRecoilValue} from 'recoil';

const ReservedBook = ({ navigation }) => {

    const userName = useRecoilValue(NameAtom)
   
    return (
        <View style={{ flex: 1, }}>
            <GeneralStatusBarColor backgroundColor={'lightgray'} name='arrow-back' iconColor='#000'  navigation = {navigation} ></GeneralStatusBarColor>
            {userName === 'axz' ? 
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>No Reserved Books</Text>
            </View>:
        
            <BooksComponent screenTitle = 'Reserved Book' heartIcon ='bookmark' date ='Date ' 
            disHeartIcon = 'ios-heart-dislike' alarm ='alarm' shareSocial ='share-social'
            > 
            </BooksComponent>}
            
        </View>


    );
};

export default ReservedBook;

