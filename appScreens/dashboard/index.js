import React, { useEffect, useState } from 'react';
import { View, ScrollView, Text, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';
import CustomInput from '../component/textInput';
import { backGroundColor } from '../assets/colors/colors';
import GeneralStatusBarColor from '../component/statusbar';
import FlatListComponent from '../component/flatListComponent';
import {API_URL} from '../../constants';
import {useRecoilState} from 'recoil';
import {BooksArrayAtom} from '../recoil/atom';


const WIDTH = Dimensions.get('window').width;

const Index = ({ navigation }) => {

    const [search, setSearch] = useState('')
    const [catIndex, setCatIndex] = useState(0)
    const [catId, setCatId] = useState(0)
    const [bookArray, setBookArray] = useRecoilState(BooksArrayAtom)
    const [fullData, setFullData] = useState([])
    const [loading, setLoading] = useState(false)
    const [catArray, setCatArray] = useState([])
   
    useEffect(() =>  {
        getCategories()
    }, [])

    const getCategories = () =>{
        fetch(API_URL + 'values/BookCategory', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                responseJson.sort((a, b) => {
                    if(a.title > b.title){
                        return 1
                    } else if(b.title > a.title){
                        return -1
                    } else {
                        return 0
                    }
                })
                // console.log(arr, 'sort');
                setCatArray(responseJson)
                getBooks(0,null,responseJson[0].id)
                setCatId(responseJson[0].id)
            })
        
            .catch((error) => {
               alert(error)
            })
    }

    const getBooks = (index, lastId, catId) => {
        
        setCatIndex(index)
        setCatId(catId)
        setLoading(true)

        fetch(API_URL + 'values/BookCategoryList?Id='+lastId+'&CategId='+catId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                // let mArray = [...bookArray, ...responseJson]
                setBookArray(responseJson)
                setFullData(responseJson)
                setLoading(false)
            })
        
            .catch((error) => {
               alert(error)
            })
    }
   
    const searchBook = (val) =>{
        setSearch(val)
        if (val.length <= 1) {
            setBookArray(fullData)
        }else {
            let searchArray = []
        for (let i = 0; i < bookArray.length; i++) {
            if (bookArray[i].title.toLowerCase().includes(search.toLowerCase())) {
                searchArray.push(bookArray[i])
                let mArray = [...[], ...searchArray]
                setBookArray(mArray)
            }
        }
        }
    }
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='transparent' name='menu' heartIcon='bookmark' navigation={navigation} ></GeneralStatusBarColor>
            <CustomInput
                hint='Search here...'
                icon='search'
                onChange={(val) => searchBook(val)}
                value={search}
                customStyle={{ width: '95%', height: 60 }}
            ></CustomInput>

            {/* categories title area */}
            <View style={{ height: 80, }}>
               <ScrollView 
               horizontal={true}
               showsHorizontalScrollIndicator={false}
               >
                
                    {
                        catArray.map((item, index)=>{
                            return (
                                <TouchableOpacity style={{flex: 1, marginLeft: 10, justifyContent: 'center',
                                 alignItems:'center', marginRight: 10}}
                                    onPress={()=>getBooks(index, null,item.id)}
                                 >
                                    <Text style={{color: index === catIndex ? 'orange' : 'black', fontWeight: 'bold'  }}>{item.title}</Text>
                                    <View style={{width: 8, height: 8, borderRadius: 8, backgroundColor: index === catIndex ? 'orange' : '#fff'}}></View>
                                </TouchableOpacity>
                            )
                        })
                    }
               </ScrollView>
            </View>
            {loading ? <ActivityIndicator size = 'large' color = {'orange'} />:
            <View style={{flex:1}}>
                    <FlatListComponent data = {bookArray} catId = {catId} index={catIndex} isLoading = {loading} navigation = {navigation}></FlatListComponent>
            </View>}
        </View>
    );
};

export default Index;
