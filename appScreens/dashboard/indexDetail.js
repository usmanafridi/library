import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, TouchableOpacity, Dimensions, Image, Alert } from 'react-native';
import CustomInput from '../component/textInput';
import GeneralStatusBarColor from '../component/statusbar';
import { backGroundColor } from '../assets/colors/colors';
import Heart from 'react-native-vector-icons/Ionicons';
import ShareSocial from 'react-native-vector-icons/Ionicons';
import Download from 'react-native-vector-icons/MaterialCommunityIcons';
import Rating from 'react-native-star-rating';
import PopupDialog from 'react-native-popup-dialog';
import { indexDtlStyle } from '../assets/style/styles';
import STAR from 'react-native-vector-icons';
import { userIdAtom } from '../recoil/atom';
import { useRecoilValue } from 'recoil';
import { API_URL } from '../../constants';
import { NameAtom } from '../recoil/atom';
import { StackActions, NavigationActions } from 'react-navigation';


const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
Heart.loadFont()

const IndexDetail = ({ navigation }) => {

    const [search, setSearch] = useState('')
    const [index, setIndex] = useState(0)
    const [fullObject, setFullObject] = useState(navigation.state.params.fullObject)
    const [popUpDialog, setPopUpDialog] = useState(false)
    const userId = useRecoilValue(userIdAtom)
    const [loading, setLoading] = useState(false)
    const userName = useRecoilValue(NameAtom)

    useEffect(() => {

    }, [loading])

    const reserveHandle = () => {

        //   if (userName === 'axz'){
        //     Alert.alert(
        //         'Login',
        //         'Please login first',
        //         [
        //           {text: 'Cancel', onPress: () => {return null}},
        //           {text: 'Ok', onPress: () => {

        //             const resetAction = StackActions.reset({
        //                 index: 0,
        //                 key: null,
        //                 actions: [NavigationActions.navigate({ routeName: 'login' })],
        //               });
        //               navigation.dispatch(resetAction);
        //           }},
        //         ],
        //         { cancelable: false }
        //       )  

        //   }if {        
        let formData = new FormData();
        formData.append('MemberCode', userId);
        formData.append('BookId', fullObject.id);

        setLoading(true)
        fetch(API_URL + 'values/BookIssue', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then((response) => response.json())
            .then((responseData) => {
                if (responseData.message === 'Success') {
                    alert('Book reserved successfully')
                    navigation.navigate('index')
                   
                    console.log(JSON.stringify(responseData))
                }
                else if(responseData.message === 'Already Reserved'){
                    alert('Book Already reserved')

                }
            })
            .catch((error) => {
                setLoading(false)
                alert('Network request failed! Please check your internet connection')
            })

    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor={'lightgray'} name='arrow-back' iconColor='#fff' navigation={navigation} ></GeneralStatusBarColor>
            <ScrollView >
                <View style={{ flex: 1, alignItems: 'center', marginTop: 10 }}>



                    <Image style={{ width: WIDTH / 4, height: HEIGHT / 4, }} source={require('../assets/images/background1.jpg')} />


                </View>

                <View style={{ flex: 1, marginTop: 10, }}>
                    {/* <View style={indexDtlStyle.secondView}> */}
                    <View style={{ width: '90%', marginTop: 30, marginLeft: 20, }}>
                        {/* <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                        Book:
                     </Text> */}
                        <Text style={{ fontSize: 15, fontWeight: 'bold', width: '80%' }}
                            multiline={true}
                        >
                            Book: {fullObject.title}
                        </Text>
                        <Text style={{ fontSize: 15, width: '80%', fontWeight: 'bold', marginTop: 10 }}>
                            Author: {fullObject.author}
                        </Text>
                    </View>

                    <View style={{ width: '90%', marginTop: 20, marginLeft: 20, }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                            Summary
                     </Text>

                        <Text >
                            Lorim ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt u incididunt mgna
                            aliqua. Ut enium ad minim venia,quid nostrued ercita tion ex ea codoc
                     </Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <TouchableOpacity
                            onPress={() => { setPopUpDialog(true) }}
                            style={{ width: 120, height: 50, justifyContent: 'center', alignItems: 'center', backgroundColor: 'orange', borderRadius: 10 }}>
                            <Text style={{ color: '#fff', fontWeight:'bold', fontSize:15 }}>RESERVE</Text>
                        </TouchableOpacity>
                    </View>
                    {/* <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <Text style={{ fontSize: 20, marginLeft:20, fontWeight: 'bold' }}>
                        Ratings
                     </Text>
                     <View>
                <Rating
                    type='custom'
                    ratingImage={STAR}
                    ratingColor ='#ffcc00'
                    ratingBackgroundColor='#fff'
                    ratingCount={5}
                    imageSize={10}
                    onFinishRating={''}
                    style={{ paddingVertical: 10,  }}
                    
                />
                 </View>
                </View> */}
                    {/* <View style={ { flexDirection:'row', marginTop: 10}}>
                    <Text style={{ fontSize: 20, marginLeft:20, fontWeight: 'bold' }}>Reviews:</Text>
                <CustomInput
                hint='Your Review'
                onChange={(val) => setSearch(val)}
                customStyle={{ width: '70%', height: 40 }}
                  ></CustomInput>
                </View> */}
                    {/* <CustomInput
                hint='Write and Submit your Review'
                icon='thumbs-up-sharp'
                onChange={(val) => setSearch(val)}
                customStyle={{ width: '75%', height: 60 }}
            ></CustomInput> */}
                </View>
                <PopupDialog
                    // ref={(popupDialog) => { this.popupDialog = popupDialog }}
                    width={350}
                    height={350}
                    visible={popUpDialog}
                >
                    <View style={{ flex: 1, backgroundColor: 'lightgray' }}>
                        <View style={{ marginLeft: 10, marginRight: 7, alignItems: 'center', marginTop: 30 }}>
                            <Heart name='bookmark' color='orange' size={50} />
                            <View style={{ marginLeft: 10, marginRight: 7, alignItems: 'center', marginTop: 30 }}>
                                <Text style={{ fontSize: 16, color: '#000', marginBottom: 10 }}>
                                    Do You Want to Reserve the Book

                            </Text>
                                <Text style={{ fontSize: 20, color: '#000', textAlign: 'center', marginBottom: 10 }}>
                                    "{fullObject.title}"

                            </Text>
                                <Text style={{ fontSize: 20, color: '#000', marginBottom: 10 }}>
                                    Genre Guaidnace

                            </Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around', marginTop: 30 }}>
                            <TouchableOpacity
                                onPress={() => { reserveHandle(); setPopUpDialog(false) }}
                                style={indexDtlStyle.popupBtnStyle}>
                                <Text style={indexDtlStyle.btnTextStyle}>Ok</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    setPopUpDialog(false)
                                }}
                                style={indexDtlStyle.popupBtnStyle}>
                                <Text style={indexDtlStyle.btnTextStyle}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </PopupDialog>
            </ScrollView>
        </View>

    );
};

export default IndexDetail;
