import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, TouchableOpacity, Image, Dimensions, StyleSheet, FlatList, StatusBar, ImageBackground } from 'react-native';
import Heart from 'react-native-vector-icons/Ionicons';
import Disheart from 'react-native-vector-icons/Ionicons'
import ShareSocial from 'react-native-vector-icons/Ionicons';
import Download from 'react-native-vector-icons/MaterialCommunityIcons';
import Alarm from 'react-native-vector-icons/Ionicons';
import { booksComponentStyle } from '../assets/style/styles';
import { userIdAtom } from '../recoil/atom';
import { useRecoilValue } from 'recoil';
import { API_URL } from '../../constants';
import moment from 'moment';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
Download.loadFont()
ShareSocial.loadFont()

const BooksComponent = ({ navigation, ...props }) => {

    const userId = useRecoilValue(userIdAtom)
    const [bookArray, setBookArray] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        fetch(API_URL + 'values/IssueBookList?id=' + userId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        })
            .then((response) => response.json())
            .then((responseJson) => {
                setBookArray(responseJson)
                setLoading(false)
            })
            .catch((error) => {
                //    alert(error)
                setLoading(false)
            })
            if (bookArray.length != 0) {
                reservedDays()
                // alert(reservedDays())
            }
    }, [loading])

    const deleteReserved = (id) => {
        let formData = new FormData();
        formData.append('MemberCode', userId);
        formData.append('BookId', id);

        setLoading(true)
        fetch(API_URL + 'values/DeleteIssueBooks', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then((response) => response.json())
            .then((responseData) => {
                if (responseData.message === 'Success') {
                    alert('Reserved book removed successfully')
                }
            })
            .catch((error) => {
                
            })
    }
    
    const reservedDays = () => {
        let current_Date = moment(new Date()).format('DD MM Y')
        bookArray.forEach(item => {
            let m = moment(item.issueDate).format('DD MM Y')
            if (parseInt(current_Date) - parseInt(m) >= 6) {
                deleteReserved(item.id, userId)
            }else {

            }
        })

        // let issue = moment(issueDate).format('YYYY/MM/DD')
        // var addDays = moment(issue).format('YYYY/MM/DD')
        // var date = moment().format('YYYY/MM/DD')
        // const date = Math.round(Math.abs((addDays.getTime() - issue.getTime())))

    }

    return (
        <View style={{ flex: 1, }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20, marginBottom: 10 }}>
                {props.screenTitle === 'Reserved Book' ?
                    <Heart name={props.heartIcon} size={30} color='#000' /> :
                    <Download name={props.disHeartIcon} size={30} color='#000' />}

                <View style={{ alignItems: 'center', marginLeft: 20 }}>
                    <Text style={{ textAlign: 'center', fontSize: 20, color: '#000' }}> {props.screenTitle}</Text>
                </View>
            </View>
            <FlatList
                data={bookArray}
                key={item => item.id}
                renderItem={({ item }) =>

                    <View style={booksComponentStyle.btnView}>
                        <View style={{ width: 90, height: 170 }}>
                            <Image style={{ height: '100%', width: '100%' }}
                                source={require('../assets/images/background1.jpg')} />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', }}>
                            <View style={{ height: HEIGHT / 8, marginTop: 5 }}>
                                <Text style={{ marginLeft: 20, marginBottom: 5 }}>Book :{item.title.length > 40 ? item.title.slice(0, 40) + '...' : item.title}</Text>
                                <Text style={{ color: 'grey', marginLeft: 20, }}>Auhtor :{item.author}</Text>
                            </View>
                            <View style={{ height: 20, width: '100%', flexDirection: 'row', marginTop: 5, marginLeft: 2, }}>
                                <Alarm name={props.alarm} size={20} color='grey' />
                                {/* {props.screenTitle === 'Reserved Book' ? 
                                null: */}
                                <Text style={{ alignItems: 'center', marginLeft: 5 }}>{props.date}{item.issueDate}</Text>
                                
                            </View>
                            <View style={{ height: 20, width: '100%', marginTop:2, marginLeft: 2, }}>
                               
                                <Text style={{ alignItems: 'center', marginLeft: 20, fontWeight:'bold', fontSize:15}}>Valid up to 6 days</Text>
                                
                            </View>
                        </View>
                        <View style={booksComponentStyle.iconView}>
                            <ShareSocial name={props.shareSocial} color='gray' size={20} />
                            {props.screenTitle === 'Reserved Book' ?
                                <TouchableOpacity onPress={() => deleteReserved(item.id)}>
                                    <Disheart name={props.disHeartIcon} color='grey' size={20} />
                                </TouchableOpacity> : null}
                            <TouchableOpacity
                                onPress={() => { }}
                            >
                                <Heart name={props.heartIcon} color='grey' size={20} />
                            </TouchableOpacity>
                        </View>
                    </View>
                }
            />
        </View>

    );
};

export default BooksComponent;