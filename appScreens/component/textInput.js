import React from 'react';
import {
    TextInput,
    View,
    StyleSheet
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { backGroundColor } from '../assets/colors/colors';

Ionicons.loadFont()

const CustomInput = (props) => {
    return (
        <View style={[styles.textInputWrapper, props.customStyle]}>
            {props.screen === 'contactus' || props.screen === 'editDetail' ? null :
            <Ionicons name={props.icon} size={20} style={{alignSelf: 'center', color: 'orange'}} />
            }
            <TextInput
                placeholder={props.hint}
                autoCapitalize="none"
                placeholderTextColor = 'orange'
                keyboardType={props.keyboard}
                multiline={props.multiline}
                secureTextEntry={props.pass}
                style={[styles.inputStyle,{textAlignVertical: props.textAlign}]}
                onChangeText={(value) => {
                    props.onChange(value);
                }}
            />
            {props.screen === 'editDetail' ? 
            <Ionicons name={props.icon} size={20} style={{alignSelf: 'center', marginRight: 20, color: 'orange'}} /> : null
        }
        </View>
    );
}

const styles = StyleSheet.create({
    textInputWrapper: {
        width: '80%',
        alignSelf: 'center',
        borderColor: '#B7B7B7',
        borderWidth: 1,
        height: 50,
        borderRadius: 6,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        flexDirection: 'row',
        margin: 5
    },
    inputStyle: {
        textAlign: 'left',
        padding: Platform.OS === 'android' ? 13 : 18,
        fontSize: 12,
        width: '100%',
        color: '#000',
    },
});

export default CustomInput;