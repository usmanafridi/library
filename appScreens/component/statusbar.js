import React from 'react';
import { View, StatusBar,StyleSheet, Platform, } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

Ionicons.loadFont();
const GeneralStatusBarColor = ({ backgroundColor,navigation, ...props }) => (
<View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} barStyle = 'dark-content' />
    <Ionicons name = {props.name} 
    onPress = {() =>{
        if (props.name === 'menu')
        {navigation.openDrawer()}
        else 
        {navigation.goBack()}
    
    }}
    color={props.iconColor} size={30} style={{marginTop: 50, marginLeft: 10 }} ></Ionicons>
    <Ionicons name = {props.heartIcon} onPress={() => {navigation.navigate('reserved')}}
    size={30} style={{marginTop: 50, marginRight: 20}} ></Ionicons>
    
</View>
);

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const styles = StyleSheet.create({
statusBar: {
height: 100,
flexDirection: 'row',
justifyContent: 'space-between'
}
});

export default GeneralStatusBarColor;
