import React, { useState, useEffect } from 'react';
import {View, Text, FlatList, Image, Dimensions, TouchableOpacity, ActivityIndicator} from 'react-native';
import { API_URL } from '../../constants';
import { useRecoilState } from 'recoil';
import { BooksArrayAtom } from '../recoil/atom';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const FlatListComponent = ({navigation, ...props}) =>{

    const [morebookArray, setMoreBookArray] = useRecoilState(BooksArrayAtom)
    const [refreshing, setRefreshing] = useState(true)
    const [refreshingArray, setRefreshingArray] = useState(props.data)

    useEffect(()=>{
        console.log('JDJDJDJDDJ', morebookArray)
    })

    const onEndReached = (lastId, catId) => {
        if (refreshingArray.length != 0 && morebookArray.length >= 49) {
            getBooks(props.index,lastId, catId)
        }
    }

    const renderFooter = () => {
        if (refreshing && refreshingArray.length != 0) {
            if (morebookArray === undefined || morebookArray.length < 50) {
                return null;
            } else {
                return <ActivityIndicator size="large" color='orange' />;
            }
        }
        else {
            return null;
        }
    }


    const getBooks = (index, lastId, catId) => {

        fetch(API_URL + 'values/BookCategoryList?Id='+lastId+'&CategId='+catId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                let mArray = [...morebookArray, ...responseJson]
                setMoreBookArray(mArray)
                setRefreshingArray(responseJson)
            })
        
            .catch((error) => {
               alert(error)
            })
    }

    return(
        <View>
            <FlatList
                data = {morebookArray}
                // key={morebookArray}
                keyExtractor={item => item.id.toString()}
                // extraData={morebookArray}
                onEndReached={item=>onEndReached(morebookArray[morebookArray.length - 1].id, props.catId)}
                onEndReachedThreshold={1}
                ListFooterComponent={renderFooter}
                // refreshing={refreshing}
                // onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                renderItem = {({item})=>
                    <TouchableOpacity 
                    style={{width:'100%'}}
                    onPress ={() => navigation.navigate('indexDtl',{
                        fullObject: item
                    })}
                    >
                        
                        <View style={{flexDirection:'row', marginLeft:10, marginRight:WIDTH/50,marginBottom:WIDTH/50, borderRadius:10, borderColor:'orange', borderWidth:3,}}>
                           
                            <Image style = {{width: WIDTH/5, height: HEIGHT/6,}} source = {require('../assets/images/background1.jpg')} />
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{alignSelf: 'center'}}>{item.title.length > 40 ? item.title.slice(0,40)+'...' : item.title}</Text>
                            <Text style={{alignSelf: 'center'}}>{item.title.length > 40 ? item.author.slice(0,40)+'...' : item.author}</Text>
                        </View>
                        </View>
                    </TouchableOpacity>
                }
            numColumns = {3}
            />
            
        </View>
        

    )
}


export default FlatListComponent