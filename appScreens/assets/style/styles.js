import { StyleSheet, Dimensions } from 'react-native';
import { backGroundColor } from '../colors/colors';

const HEIGHT = Dimensions.get('window').height;

// login and signup style
export const userStyle = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#009387',
    },

})
export const booksComponentStyle = StyleSheet.create({
    btnView: {
        width: '90%', height: 170, backgroundColor: 'white', alignSelf: 'center',
        shadowColor: backGroundColor, bottom: 10, shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.23,
        shadowRadius: 2.62, elevation: 10, marginTop: 10, flexDirection: 'row'
    },
    iconView: {
        height: 40, width: 80, flexDirection: 'row', justifyContent: 'space-around', marginRight: 10, marginTop: 20
    },
})
export const indexDtlStyle = StyleSheet.create({
    
        iconsView:{ flex: 1,  alignItems: 'center',flexDirection:'row', marginRight:10, justifyContent: 'flex-end'},
        secondView:{justifyContent: 'center', alignItems: 'center', width:'90%', marginTop: 20 },
        popupBtnStyle:{ width: 80, height: 40, justifyContent: 'center', borderRadius: 5, backgroundColor: 'orange' },
        btnTextStyle:{ alignSelf: 'center',fontSize:18 ,fontWeight:'bold',color:'#fff'},
    
})
export const drawerStyle = StyleSheet.create({
    viewStyle:{
     width: '100%', alignItems: 'center', flexDirection: 'row', marginLeft:30, 
    },
    header: {
        marginLeft: 10, marginTop: 5, flexDirection: 'row',
    },
    headerText: {
        fontSize: 15, marginLeft: 10
    },
    contentView: {
        justifyContent: 'center', alignItems: 'center',
    },
    contentText: {
        fontSize: 12, fontWeight: 'bold', textAlign: 'justify', margin: 10, width: 130
    },
    viewText:{ 
        marginLeft: 10, fontSize: 20, color:'grey'
    },
})