import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, TouchableOpacity, Dimensions, StyleSheet, StatusBar,TextInput ,ImageBackground } from 'react-native';
import CustomInput from '../component/textInput';
import GeneralStatusBarColor from '../component/statusbar';
import { backGroundColor } from '../assets/colors/colors';
import {userIdAtom} from '../recoil/atom';
import {useRecoilValue} from 'recoil';
import {API_URL} from '../../constants';


const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const ContactUs = ({ navigation }) => {
    const [loading, setLoading] = useState(false)
    const [message, setMessage] = useState('')
    const userId = useRecoilValue(userIdAtom)

    useEffect(()=>{

    },[loading] )
  
  const contactHandle = () => {
    if (message === '' ) {
      alert('Write your message')
   }
   else {  
      
        let formData = new FormData();
        formData.append('MemberCode',userId);
        formData.append('Messages',message);
        
          setLoading(true)
          fetch(API_URL + 'values/Contents', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data'
            },
           body:formData
          }).then((response) => response.json())
          .then((responseData) => {
            if (responseData.message === 'Success'){
              alert('Message submitted successfully')
              setLoading(false)
            }
            
          })
              .catch((error) => {
                setLoading(false)
                alert(error)
          })
        }
      
  
  }

    return (
        <ScrollView style={{ flex: 1, }}>
            <GeneralStatusBarColor backgroundColor={backGroundColor} name='arrow-back' iconColor='#fff' navigation = {navigation} ></GeneralStatusBarColor>
           <View style={{flex: 1, alignItems: 'center'}}>
              <Text style={{ textAlign: 'center', fontSize: 20, fontWeight:'bold', color: '#003f15', marginTop: 30 }}>Contact Us</Text>
                 <CustomInput  
                  icon='mail'
                  customStyle={{ width: '80%', height: HEIGHT/2, marginTop: HEIGHT/25}}
                  hint='Write your message'
                  keyboard=''
                  multiline={true}
                  pass={false}
                  textAlign='top'
                  screen='contactus'
                  onChange={(val) => setMessage(val)}
                ></CustomInput>
                
                <TouchableOpacity onPress={() =>contactHandle()}
                style={{height:50,width:'40%',borderRadius:5,justifyContent:'center',backgroundColor:backGroundColor, marginTop: 20}}>
                    <Text style={{fontSize:20,fontWeight:'bold',color:'#fff',textAlign:'center',}}> {loading ? 'Submitting ...' : 'SUBMIT'}</Text>
                </TouchableOpacity>
                {/* <Text style={{fontSize:15,color:backGroundColor, marginTop: 20}}>Subscribe to get updates about new books</Text> */}
           </View>
        </ScrollView>

    );
};

export default ContactUs;