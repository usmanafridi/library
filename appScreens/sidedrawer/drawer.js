import React, { useState } from 'react';
import { View,  TouchableOpacity, Alert, Image, Dimensions, AsyncStorage, } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Download from 'react-native-vector-icons/MaterialCommunityIcons';
import Checkcircle from 'react-native-vector-icons/Feather';
import Logout from 'react-native-vector-icons/SimpleLineIcons';
import { backGroundColor } from '../assets/colors/colors';
import {drawerStyle} from '../assets/style/styles'
import {useRecoilValue} from 'recoil';
import {NameAtom} from '../recoil/atom'
import {Text} from 'native-base';

    Ionicons.loadFont();
    Logout.loadFont();
    Download.loadFont();
    Checkcircle.loadFont();
    const HEIGHT = Dimensions.get('window').height

    const SideDrawer = ({ navigation }) => {
        const userName = useRecoilValue(NameAtom)
    
        const logOut = () =>{
            Alert.alert(
                'Log out',
                'Do you want to logout?',
                [
                  {text: 'Cancel', onPress: () => {return null}},
                  {text: 'Ok', onPress: () => {
                    AsyncStorage.clear();
                    navigation.navigate('login')
                  }},
                ],
                { cancelable: false }
              )  
        
    }    

     return (
        <View style={{ flex: 1, }}>
            <View style={{ width: '100%', height: HEIGHT / 5, alignItems: 'center', marginTop:30,backgroundColor:'lightgray', flexDirection:'row' }}>
                <Image style={{ width: 50, height: 50, marginLeft: 20, borderRadius: 50 / 2, backgroundColor: '#fff' }} 
                source={require('../../appScreens/assets/images/login.jpeg')} />
                <Text note style={drawerStyle.viewText}>{userName}</Text>
            </View>

            <TouchableOpacity style={[drawerStyle.viewStyle,{marginTop: 10}]}
            onPress = {() => navigation.navigate('editDetail')}>
                <Ionicons name = 'person' size = {20} color ={'grey'} />
                <Text note style={drawerStyle.viewText}>Edit Personal Details</Text>
            </TouchableOpacity>
            <View style={{ marginLeft: 20, borderWidth: .5, marginTop: 5, width: ('90%') }}/>
            

            <TouchableOpacity style={[drawerStyle.viewStyle,{marginTop: 10}]}
            onPress={() => navigation.navigate('issued')}
            >
                 <Download name = 'download-box' size = {20} color ={'grey'} />
                <Text note style={drawerStyle.viewText}>Issued Books</Text>
            </TouchableOpacity>
            <View style={{ marginLeft: 20, borderWidth: .5, marginTop: 5, width: ('90%') }}/>
            

            <TouchableOpacity onPress={() => navigation.navigate('reserved')}
            style={[drawerStyle.viewStyle,{marginTop: 10}]}>
                 <Ionicons name = 'bookmark' size = {20} color ={'grey'} />
                <Text note style={drawerStyle.viewText}>Reserved Books</Text>
            </TouchableOpacity>
            <View style={{ marginLeft: 20, borderWidth: .5, marginTop: 5, width: ('90%') }}/>
           

            <TouchableOpacity onPress={() => navigation.navigate('')}
            style={[drawerStyle.viewStyle,{marginTop: 10}]}>
            <Checkcircle name = 'check-circle' size = {20} color ={'grey'} />
                <Text note style={drawerStyle.viewText}>Terms & condition</Text>
            </TouchableOpacity>
            <View style={{ marginLeft: 20, borderWidth: .5, marginTop: 5, width: ('90%') }}/>
           

            <TouchableOpacity onPress={() => navigation.navigate('')}
           style={[drawerStyle.viewStyle,{marginTop: 10}]}>
            <Ionicons name = 'help' size = {20} color ={'grey'} />
                <Text note style={drawerStyle.viewText}>Help</Text>
            </TouchableOpacity>
            <View style={{ marginLeft: 20, borderWidth: .5, marginTop: 5, width: ('90%'),backgroundColor:'grey' }}/>
            

            <TouchableOpacity onPress={() => navigation.navigate('contact')}
              style={[drawerStyle.viewStyle,{marginTop: 10}]}>
                <Ionicons name = 'call' size = {20} color ={'grey'} />
                <Text note style={drawerStyle.viewText}>Contact Us</Text>
            </TouchableOpacity>
            <View style={{ marginLeft: 20, borderWidth: .5, marginTop: 5, width: ('90%') }}/>
           

            <TouchableOpacity onPress={() => logOut()}
              style={[drawerStyle.viewStyle,{marginTop: 10}]}>
                <Logout name = 'logout' size = {20} color ={'grey'} />
                <Text note style={drawerStyle.viewText}>Log Out</Text>
            </TouchableOpacity>

        </View>
    )
}


export default SideDrawer;
