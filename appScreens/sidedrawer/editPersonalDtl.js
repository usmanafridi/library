import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, TouchableOpacity, Dimensions, Alert, StyleSheet, TextInput } from 'react-native';
import GeneralStatusBarColor from '../component/statusbar';
import { backGroundColor } from '../assets/colors/colors';
import { userIdAtom } from '../recoil/atom';
import { useRecoilValue } from 'recoil';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { API_URL } from '../../constants';

// FontAwesome5.loadFont()

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const EditDetail = ({ navigation }) => {
    const userId = useRecoilValue(userIdAtom)
    const [name, setName] = useState(null)
    const [email, setEmail] = useState(null)
    const [phone, setPhone] = useState(null)
    const [organization, setOrganization] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        getUserInfo()

    }, [loading])

    const getUserInfo = () => {

        fetch(API_URL + 'values/ShowUsers?MemberCode=' + userId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                // alert(JSON.stringify(responseJson))
                setName(responseJson[0].name)
                setEmail(responseJson[0].email)
                setOrganization(responseJson[0].organization)
                setPhone(responseJson[0].mobile)
                setLoading(false)
            })
            .catch((error) => {
                //    alert(error)
                setLoading(false)
            })
    }
    const validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
          alert("Email is invalid");
          return false;
        }
        else {
          getUserUpdate()
          return true;
        }
      }

    const getUserUpdate = () => {
        if (name === null || email === null || organization === null || phone === null) {
            Alert.alert(
                'Wrong Input!',
                'Fields cannot be empty.',
                [{ text: 'Okay' }],
            );
        }
        else {
            let formData = new FormData();
            formData.append('Name', name);
            formData.append('Organization', organization);
            formData.append('Mobile', phone);
            formData.append('Email', email);
            formData.append('MemberCode', userId);

            setLoading(true)
            fetch(API_URL + 'values/SignUpUpdate', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: formData
            }).then((response) => response.json())
                .then((responseData) => {
                if(responseData.message === 'Data successfully updated '){
                    alert('User update successfully')
                }
            //  alert(JSON.stringify(responseData))
                getUserInfo()

                })
                .catch((error) => {
                    setLoading(false)
                    alert('Network request failed! Please check your internet connection')
                })
        }
    }



    return (
        <ScrollView style={{ flex: 1, }}>
            <GeneralStatusBarColor backgroundColor={'lightgray'} name='arrow-back' iconColor='#000' navigation={navigation} ></GeneralStatusBarColor>
            <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={{ textAlign: 'center', fontSize: 20, fontWeight: 'bold', color: '#000', marginTop: 30 }}>Edit Detail</Text>

                <View style={styles.textInputWrapper}>
                    <Text style={{ alignSelf: 'center', width: 50 }}>Email</Text>
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={(val) => setEmail(val)}
                        value={email}
                    ></TextInput>
                    <FontAwesome5 name='edit' size={20} />
                </View>

                <View style={styles.textInputWrapper}>
                    <Text style={{ alignSelf: 'center', width: 50 }}>Name</Text>
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={(val) => setName(val)}
                        value={name}
                    ></TextInput>
                    <FontAwesome5 name='edit' size={20} />
                </View>

                <View style={styles.textInputWrapper}>
                    <Text style={{ alignSelf: 'center', width: 50 }}>Org</Text>
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={(val) => setOrganization(val)}
                        value={organization}
                    ></TextInput>
                    <FontAwesome5 name='edit' size={20} />
                </View>

                <View style={styles.textInputWrapper}>
                    <Text style={{ alignSelf: 'center', width: 50 }}>Phone</Text>
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={(val) => setPhone(val)}
                        value={phone}
                    ></TextInput>
                    <FontAwesome5 name='edit' size={20} />
                </View>

                <TouchableOpacity onPress={() => validate(email)}
                    style={{ height: 50, width: '40%', borderRadius: 5, justifyContent: 'center', backgroundColor: 'orange', marginTop: 20 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#fff', textAlign: 'center', }}>Update</Text>
                </TouchableOpacity>
                {/* <Text style={{fontSize:15,color:backGroundColor, marginTop: 20}}>Subscribe to get updates about new books</Text> */}
            </View>
        </ScrollView>

    );
};
const styles = StyleSheet.create({
    textInputWrapper: {
        width: '80%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#B7B7B7',
        borderWidth: 1,
        height: 50,
        borderRadius: 6,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        flexDirection: 'row',
        margin: 5
    },
    inputStyle: {
        textAlign: 'left',
        padding: Platform.OS === 'android' ? 13 : 18,
        fontSize: 12,
        width: '80%',
        color: 'orange',
    },
});


export default EditDetail;
